from django.db import models
from django.db.models import Min, Sum, Max, F, Count
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from abc import ABCMeta, abstractmethod
import random


class Client:
    __metaclass__ = ABCMeta

    @abstractmethod
    def buyStock(self, stock, quantity):
        "a metod to implement"

    @abstractmethod
    def addFunds(self, value):
        "a method to implement"

    @abstractmethod
    def sellStock(self, stock, quantity):
        "a method to implement"


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Market(metaclass=SingletonMeta):
    name = "Antigua"

    def getMarketName(self):
        return self.name

    def updatePrices(self):
        stocks = Stock.objects.all()

        for stock in stocks:
            new_price = stock.calculateNextMonthPrice()
            stock.valoare = new_price
            stock.save()

    def barInvestors(self):
        investors = Person.objects.all()

        for investor in investors:
            if not investor.is_barred:
                if not investor.is_playing:
                    investor.is_barred = True
                    investor.save()
                else:
                    chosen_stock = investor.reserved_stock
                    stock = Stock.objects.get(name=chosen_stock)
                    if stock.generateV() <= 5:
                        investor.is_barred = True
                        investor.save()
            else:
                investor.is_barred = False
                investor.save()

    def reserveStockForAuto(self):
        investors = Person.objects.all()
        for investor in investors:
            if investor.autoPlay:
                investor.reserveAutoPlay(investor.reserved_stock)


    def buyStockForAllPlaying(self):
        investors = Person.objects.all()
        for investor in investors:
            if investor.is_playing:
                stock = Stock.objects.get(name=investor.reserved_stock)
                if(investor.funds>=stock.valoare):
                    investor.funds=investor.funds-stock.valoare;
                    investor.save()

    def sellAllReservedStocks(self):
        investors = Person.objects.all()

        for investor in investors:
            if investor.is_playing:
                if not investor.autoPlay:
                    investor.sellReservedStock()

    def sellForAuto(self):
        investors=Person.objects.all()

        for investor in investors:

            stock = Stock.objects.get(name=investor.reserved_stock)

            if investor.autoPlay and investor.is_playing:
                investor.sellAutoPlay()




class Stock(models.Model):
    name = models.CharField(max_length=30, blank=True);
    valoare = models.FloatField(default=0)
    max_value = models.FloatField(default=0)

    def __str__(self):
        return self.name

    def getStockbyName(self, name):
        return Stock.objects.get(name=name)

    def getStockbyId(self, id):
        return Stock.objects.get(id=id)

    def getMaxValue(self):
        return self.max_value;

    def generateV(self):
        if self.valoare == 0:
            return 0;
        return int((self.max_value * (self.valoare / self.max_value)) / 10)

    def calculateNextMonthPrice(self):

        prob = random.randint(1, 100);
        next_value = 0;
        v = self.generateV()

        if v < 5:
            if prob <= 66:
                next_value = self.valoare + self.max_value / 10;
            else:
                next_value = self.valoare - self.max_value / 10;

        if v == 5:
            if prob <= 50:
                next_value = self.valoare + self.max_value / 10;
            else:
                next_value = self.valoare - self.max_value / 10;

        if v > 5:
            if prob <= 33:
                next_value = self.valoare + self.max_value / 10
            else:
                next_value = self.valoare - self.max_value / 10;

        if next_value > self.max_value:
            next_value = self.max_value

        if next_value < 0:
            next_value = 0

        return next_value


class Person(models.Model, Client):
    funds = models.FloatField(default=0)  # to be implemented
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=30);
    owned_stocks = models.ManyToManyField('Stock')
    total_value = models.FloatField(default=0)

    is_barred = models.BooleanField(default=False)
    is_playing = models.BooleanField(default=False)
    reserved_stock = models.CharField(max_length=30, null=True)
    autoPlay=models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def getPersonbyId(self, id):
        return Person.objects.get(id=id)

    def addFunds(self, value):
        self.funds = self.funds + value
        self.save()

    def deductFunds(self, value):
        self.funds -= value
        self.save()

    def buyStock(self, stock, quantity):
        ord = Order(person=self, stock=stock, quantity=quantity)
        ord.save()
        print("$ ", self.funds, "remaining in account \n")

    # to be implemented
    def sellStock(self, stock, quantity):
        pass

    def sellReservedStock(self):
        self.is_playing = False

        if self.reserved_stock:
            if Stock.objects.get(name=self.reserved_stock):
                stock = Stock.objects.get(name=self.reserved_stock)
                self.funds += stock.valoare
                self.save()

    def reserveStock(self, stockName):
        self.reserved_stock = stockName
        self.is_playing = True
        self.funds -= Stock.objects.get(name=stockName).valoare
        self.save()

    def reserveAutoPlay(self,stockName):
        self.reserved_stock=stockName
        stock = Stock.objects.get(name=self.reserved_stock)

        if (stock.generateV() <= 5):
            self.is_playing=True
        else:
            self.is_playing=False
        self.save()

    def sellAutoPlay(self):
        if self.reserved_stock:
            if Stock.objects.get(name=self.reserved_stock):
                stock = Stock.objects.get(name=self.reserved_stock)

                self.funds += stock.valoare
                self.is_playing=True
                self.save()


class PersonProxy(Client):

    def __init__(self, user):
        self.ProxyPerson = Person.objects.all().get(user=user)

    def addFunds(self, value):
        self.ProxyPerson.addFunds(value)

    def buyStock(self, stock, quantity):

        if stock.valoare * quantity < self.ProxyPerson.funds:
            self.ProxyPerson.buyStock(stock, quantity)
            self.ProxyPerson.deductFunds(stock.valoare * quantity)
        else:
            print("Insuficient funds\n")

    # to be implemented
    def sellStock(self, stock, quantity):
        pass


class Order(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE);
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE);
    quantity = models.FloatField(default=0);

    # valoare totala stocks
    def totalCost(self, user):
        total = self.filter(person__user=user).aggregate(suma=Sum(F('quantity') * F('stock__valoare')))

        return total

    # lista individual orders, cu pret total pe comanda
    def individual_orders(self, user):
        indiv = self.filter(person__user=user).annotate(total_value=Sum(F('quantity') * F('stock__valoare'))
                                                        , nume_stock=F('stock__name'),
                                                        cost_per_stock=F('stock__valoare'),
                                                        total_quantity=F('quantity'))
        return indiv

    # lista de stocks, cu total detinute in functie de stock_id
    def total_by_stock(self, user):
        return self.filter(person__user=user).values('stock_id').order_by('stock_id').annotate(
            total_quantity=Sum('quantity'),
            nume_stock=F('stock__name'),
            cost_per_stock=F('stock__valoare'),
            total_value=Sum(F('quantity') * F('stock__valoare')))
