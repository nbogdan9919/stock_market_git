# Generated by Django 3.1.3 on 2020-12-30 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0003_auto_20201230_1609'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='reserved_stock',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
