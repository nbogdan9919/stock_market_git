# Generated by Django 3.1.3 on 2020-12-30 14:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_auto_20201230_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='is_barred',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='person',
            name='is_playing',
            field=models.BooleanField(default=False),
        ),
    ]
