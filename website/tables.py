import django_tables2 as tables
from .models import Stock
from .models import Person
from .models import Order


class StockTable(tables.Table):
    class Meta:
        model = Stock
        template_name = "django_tables2/bootstrap.html"
        fields = ("name", "valoare","max_value", "acc_age")


class PersonTable(tables.Table):
    class Meta:
        model = Person
        template_name = "django_tables2/bootstrap.html"
        fields = ("name", "owned_stocks")

class OrderHistoryTable(tables.Table):
    class Meta:
        model=Order
        template_name="django_tables2/bootstrap.html"
        fields=("nume_stock","total_quantity","cost_per_stock","total_value")


