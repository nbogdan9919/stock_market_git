from django.contrib.auth import login, logout
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django_tables2 import SingleTableView
from django.contrib.auth.models import User
from website.models import *
from django.views.generic import ListView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
# Create your views here.
from website.tables import *
from website.forms import *


def findBestStock(allStock):
    bestStock = Stock.getStockbyId(allStock, 1)

    for stockk in allStock:
        if stockk.generateV() <= 5:  # daca are probabilitate mare sa creasca
            if stockk.getMaxValue() > bestStock.getMaxValue():  # si valoare mai mare cu care creste
                bestStock = stockk;
        else:
            if bestStock.generateV() > 5 and bestStock.generateV < 10:  # altfe, daca are valoare mai mica sa creasca, se verifica
                if stockk.getMaxValue() > bestStock.getMaxValue():  # ca nu cumva maximul sa fie deja un stock
                    bestStock = stockk;  # cu o valoare mai buna

    return bestStock;


class Index:

    def index(request):
        allPersons = Person.objects.all()
        allStocks = Stock.objects.all()
        allOrders = Order.objects.all()

        # individual_Orders = Order.individual_orders(allOrders, 1);
        # totalCost = Order.totalCost(allOrders, 1)
        context = {'allPersons': allPersons, 'allStocks': allStocks, 'allOrders': allOrders}
        # 'individualOrders': individual_Orders.values,
        # 'totalCost': totalCost}
        return render(request, 'website/index.html', context)


def homepage(request):
    if not request.user.is_authenticated:
        return render(request, 'website/home.html')
    else:
        username = request.user.username
        context = {
            'user': username
        }
        return render(request, 'website/home_logged_in.html', context)


def show_market(request):
    market1 = Market()
    market2 = Market()

    context = {'name1': market1.getMarketName(), 'id1': id(market1), 'name2': market2.getMarketName(),
               'id2': id(market2)}
    return render(request, 'website/show_market.html', context)


class TabelStock(SingleTableView):
    model = Stock
    table_class = StockTable
    allStock = Stock.objects.all

    template_name = 'website/stocks.html'


class TabelPerson(SingleTableView):
    model = Person
    table_class = PersonTable
    template_name = 'website/person.html'


def register_request(request):
    context = {}
    form = ExtendedUserCreationForm(request.POST or None)
    person_form = PersonForm(request.POST or None)
    if request.method == "POST":
        print(form.is_valid(), " ", person_form.is_valid())
        print(form)
        print(person_form)
        if form.is_valid() and person_form.is_valid():
            user = form.save()
            person = person_form.save(commit=False)
            person.user = user

            person.total_value = 0
            person.save()

            login(request, user)
            return render(request, 'website/home_logged_in.html')
    context['form'] = form
    context['person_form'] = person_form
    return render(request, 'registration/register.html', context)


def logout_request(request):
    logout(request)


def add_funds(request):
    if request.user.is_authenticated:

        funds = request.POST.get('funds')
        allPersons = Person.objects.all()

        user = request.user
        pers = allPersons.get(user=user)

        context = {'stocks': allPersons,
                   'person': pers}

        if funds and funds.isnumeric():
            if float(funds) > 0:
                personProxy = PersonProxy(user)
                personProxy.addFunds(float(funds))
                return HttpResponseRedirect("/add_funds/")

        return render(request, 'buy/add_funds.html', context)
    else:
        return render(request, 'website/home.html')


def buy_stock(request):
    if request.user.is_authenticated:

        allStocks = Stock.objects.all()
        allPersons = Person.objects.all()
        context = {'stocks': allStocks}

        stock = request.POST.get('stock')
        quantity = request.POST.get('quantity')

        if (quantity):
            user = request.user
            pers = allPersons.get(user=user)
            personProxy = PersonProxy(user)

            stck = Stock.getStockbyName(allStocks, stock)
            personProxy.buyStock(stck, float(quantity))

        return render(request, 'buy/buy_stock.html', context)

    else:
        return render(request, 'website/home.html')



def seeStocks(request):
    allStocks = Stock.objects.all()

    table_Total_Stock = StockTable(allStocks);
    bestStock = findBestStock(allStocks)

    context = {
        'allStocks': allStocks,
        'bestStock': bestStock,
        'table_stock': table_Total_Stock}

    return render(request, 'website/stocks.html', context)


def check_stock(request):
    if request.user.is_authenticated:

        allOrders = Order.objects.all()
        allStocks = Stock.objects.all();

        user = request.user
        individual_Orders = Order.individual_orders(allOrders, user);
        totalCost = Order.totalCost(allOrders, user)
        totalStock = Order.total_by_stock(allOrders, user)

        table_Individual_Orders = OrderHistoryTable(individual_Orders)
        table_Total_Stock = OrderHistoryTable(totalStock);

        bestStock = findBestStock(allStocks)

        context = {'order': allOrders,
                   'individualOrders': individual_Orders.values,
                   'totalCost': totalCost,
                   'allStocks': allStocks,
                   'totalStock': totalStock,
                   'bestStock': bestStock,
                   'table': table_Individual_Orders,
                   'table2': table_Total_Stock}

        return render(request, 'check/stock_info.html', context)
    else:
        return render(request, 'website/home.html')


def view_account(request):
    if request.user.is_authenticated:
        user = request.user
        account_info = Person.objects.get(user=user)
        context = {
            'user': user,
            'account': account_info
        }
        return render(request, 'website/your_account.html', context)
    else:
        return render(request, 'website/home.html')


def reserve_stock(request):
    if request.user.is_authenticated:
        allStocks = Stock.objects.all()

        user = request.user
        person = Person.objects.get(user=user)

        context = {'stocks': allStocks,
                   'person': person}

        stock = request.POST.get('stock')
        if stock:
            client = Person.objects.get(user=request.user)
            if not client.is_playing and not client.is_barred:
                if client.autoPlay==False:
                    client.reserveStock(stock)
                else:
                    client.reserveAutoPlay(stock)
            if(client.autoPlay==True):
                client.reserveAutoPlay(stock)

        return render(request, 'website/reserve_stock.html', context)
    else:
        return render(request, 'website/home.html')


# metoda ta boss
def month_passed(request):
    market = Market()
    market.reserveStockForAuto()
    market.buyStockForAllPlaying()
    market.updatePrices()
    market.sellAllReservedStocks()
    market.sellForAuto()

    market.barInvestors()

    return render(request, 'website/home.html')


def change_autoplay(request):
    if request.user.is_authenticated:
        invester = Person.objects.get(user=request.user)
        invester.autoPlay = not invester.autoPlay
        invester.save()
        return reserve_stock(request)
