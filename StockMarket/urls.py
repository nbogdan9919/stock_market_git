"""StockMarket URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from website import views
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [

    path('admin/', admin.site.urls),
    path('', views.homepage),
   # path('stocks/', views.TabelStock.as_view()),
    path('stocks/',views.seeStocks),
    path('person/', views.TabelPerson.as_view()),
    #path('test/', views.Index.index),
    path('market/', views.show_market),
    path('test/', views.month_passed),
    path('reserve/', views.reserve_stock, name='reserve'),
    path('autoplay/', views.change_autoplay),

    path("registration/register/", views.register_request, name="register"),
    path("registration/", include('django.contrib.auth.urls')),
    path("registration/logout/", views.logout_request, name="logout"),


    path('add_funds/',views.add_funds,name='addFunds'),
    path('buy/',views.buy_stock,name='buy'),
    path('check/',views.check_stock,name='check_stock'),
]
